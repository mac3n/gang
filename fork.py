"""
    unilities for multiway fork/kill/join
"""
import os
import sys
import signal


# tracing
TRACE = None


# we index forks 1..N so they're all nonzero
# the root has index None

def fork(pids, forks):
    "Fork and return index."
    while len(pids) < forks:
        pid = os.fork()
        if not pid:
            # forked process
            return len(pids) + 1
        # parent process
        pids.append(pid)
        if TRACE:
            TRACE("fork[%d]: %d", len(pids), pid)
    return None


def join(pids=None):
    "Reap forks and return failures."
    if pids is None:
        # individual process
        if TRACE:
            TRACE("join")
        sys.exit(0)
    fails = []
    # reap only our PIDs
    if TRACE:
        TRACE("joins")
    for pid in pids:
        (pid, exit) = os.waitpid(pid, 0)
        if exit:
            fails.append(exit)
            if TRACE:
                TRACE("fail %d", exit)
    return fails


def kill(pids, sig=signal.SIGHUP):
    "hang up on forks."
    if pids is None:
        # individual process
        os.kill(0, sig)
    if TRACE:
        TRACE("kills")
    for pid in pids:
        os.kill(pid, sig)
