import time

import fork

# don't use nosetests here, as we fork processes


def forktest(n=20):
    pids = []
    i = fork.fork(pids, n)
    if i:
        time.sleep(i * 0.5)
        fork.join()
    else:
        fork.join(pids)


def killtest(n=20):
    pids = []
    i = fork.fork(pids, n)
    if i:
        time.sleep(5)
        fork.join()
    else:
        fork.kill(pids)
        fork.join(pids)

if __name__ == "__main__":
    import trace

    fork.TRACE = trace.trace
    forktest()
    killtest()
