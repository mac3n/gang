from __future__ import print_function

import pickle
import time

import gang
import genred


def test_genred_serial(n=5, sleep=None):
    "Test gen/redwithout gang dispatch."
    r = []
    print("no forks", "gen:",  n, "sleep:", sleep)
    t = time.time()
    with genred.GenRed(None, iter(range(n)), lambda i: r.append(i)) as (gen, red):
        for i in gen:
            print("work", i)
            red(i)
            if sleep:
                time.sleep(sleep)
    t = time.time() - t
    print("done:", "%.3fs" % t)
    # result can be disordered
    assert sorted(r) == [i for i in range(n)]


def test_genred_empty(forks=1):
    "Test empty generate."
    print("forks:", forks, "no gen:")
    t = time.time()
    g = gang.Gang(forks)
    with genred.GenRed(g, iter([]), None) as (gen, red):
        for i in gen:
            print("work", i)
            red()
    t = time.time() - t
    print("done:", "%.3fs" % t)


def test_genred_parallel(forks=1, n=10, sleep=0):
    "Test full dispatch."
    print("forks:", forks, "gen:", n, "sleep:", sleep)
    t = time.time()
    g = gang.Gang(forks, codec=pickle, size=512)
    r = []
    with genred.GenRed(g, iter(range(n)), lambda i: r.append(i)) as (gen, red):
        for i in gen:
            print(g.tap, "work", i)
            red(i)
            if sleep:
                time.sleep(sleep)
    t = time.time() - t
    print("done:", "%.3fs" % t)
    # result can be disordered
    assert sorted(r) == [i for i in range(n)]


def test_genred_err(forks=1, n=10):
    "Test gen/red desync."
    g = gang.Gang(forks, codec=pickle)
    fail = None
    try:
        with genred.GenRed(g, iter(range(2)), print) as (gen, red):
           for i in gen:
                print("work", i)
                red(i)
                red(i)
    except gang.GangError as e:
        fail = e
    print("expected:", fail)
    assert fail


def test_genred_nest(forks1=1, forks2=1, n=10, sleep=0):
    "Test nested gangs."
    print("fork1:", forks1, "forks2:", forks2, "gen:", n, "sleep:", sleep)
    g1 = gang.Gang(forks1, codec=pickle, size=512) if forks1 else None
    g2 = gang.Gang(forks2, codec=pickle, size=512) if forks2 else None
    t = time.time()
    r = []
    with genred.GenRed(g1, iter(range(n)), lambda i: r.append(i)) as (gen1, red1):
        print("g1:", g1.tap if g1 else None)
        if not g1 or g1.sub:
            with genred.GenRed(g2, gen1, red1) as (gen2, red2):
                print("g1:", g1.tap if g1 else None, "g2:", g2.tap if g2 else None)
                for i in gen2:
                    print("work", i)
                    red2(i)
                    if sleep:
                        time.sleep(sleep)
    t = time.time() - t
    print("done:", "%.3fs" % t)
    # result can be disordered
    assert sorted(r) == [i for i in range(n)]


if __name__ == "__main__":
    import bus
    import fork
    import trace

    gang.TRACE = bus.TRACE = fork.TRACE = trace.trace

    if True:
        test_genred_serial(sleep=1)
        test_genred_empty(forks=5)
        test_genred_parallel(forks=1, n=5, sleep=1)
        test_genred_parallel(forks=5, n=1, sleep=1)
        test_genred_parallel(forks=10, n=20)
        test_genred_err()
    else:
        test_genred_nest(forks1=0, forks2=0)
        test_genred_nest(forks1=0, forks2=2, n=5)
        test_genred_nest(forks1=1, forks2=1, n=1, sleep=1)
