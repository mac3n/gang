"""
    Message bus using a reliable unix packet stream
    with a single Top end and multiple Tip ends.
    Packets are limited-size
    Messages are nonempty unlimited size, in full packets ending with
    a short packet.

    Packet Size can be quite large (> 100kB) on linux,
    but that requires large recv allocations

    An empty message is a FIN end of messages signal.
    The shared Tip ends use fcntl locking to keep messages atomic.
"""
import socket
import lockf

# tracing
TRACE = None


# we use this instead of a stream pipe to mark and flush on message boundaries
def pair():
    "Reliable packetized stream."
    return socket.socketpair(socket.AF_UNIX, socket.SOCK_SEQPACKET)


class Tap(object):
    """
        Message bus protocol.
        Messages are packetized in Unix datagrams, with a short packet at the end.
        We can block on reads, because we know that a message is written in a burst.
        An empty message signals the end of the stream.
    """
    __slots__ = ["sock", "size", "tip", "fins"]

    def __init__(self, sock, size):
        self.sock, self.size = sock, size
        self.tip = None
        self.fins = 0

    def __repr__(self):
        "identify."
        return "tap"

    def send(self, msg):
        "Write as series of packets."
        if not len(msg):
            raise ValueError("empty message")
        mem = memoryview(msg)
        offs, sizes = 0, []
        while offs <= len(msg):
            pkt = mem[offs:offs + self.size].tobytes()
            self.sock.send(pkt)
            sizes.append(len(pkt))
            offs += self.size
        if TRACE:
            TRACE("%r send(%s)", self, "+".join(str(s) for s in sizes))

    def recv(self):
        "Read series of packets."
        msg, sizes = b"", []
        while True:
            pkt = self.sock.recv(self.size)
            msg += pkt
            sizes.append(len(pkt))
            if len(pkt) < self.size:
                # short packet for EOM
                break
        if msg:
            if TRACE:
                TRACE("%r recv(%s)", self, "+".join(str(s) for s in sizes))
            return msg
        else:
            # empty recv is FIN
            if TRACE:
                TRACE("%r recv(FIN)", self)
            self.fins += 1
            return None

    def fin(self):
        "Send empty message as FIN."
        self.sock.send(b"")
        if TRACE:
            TRACE("%r send(FIN)", self)

    def close(self):
        "Close sock end."
        self.sock.close()


# process message bus taps
# there's only one top, so it doesn't need locking

class Top(Tap):
    "Bus root."

    def __init__(self, sock, size):
        Tap.__init__(self, sock, size)
        if TRACE:
            TRACE("%r fd: %d", self, self.sock.fileno())

    def __repr__(self):
        "identify."
        return "top"

    def fin(self, pids):
        "Send FIN for each edge."
        for _ in pids:
            Tap.fin(self)


# tips have to lock
class Tip(Tap):
    "Bus edge, requires locks for possible multipacket messages."

    __slots__ = ("rlock", "wlock")

    def __init__(self, sock, size, locks):
        "Init with index."
        Tap.__init__(self, sock, size)
        (self.rlock, self.wlock) = locks
        # should be updated with index
        self.tip = 0
        if TRACE:
            TRACE("%r fd: %d", self, self.sock.fileno())

    def __repr__(self):
        "identify."
        return "tip[%d]" % self.tip

    # lock keeps packets from mixing messages

    def send(self, msg):
        "Write with lock."
        with self.wlock:
            Tap.send(self, msg)

    def fin(self):
        with self.wlock:
            Tap.fin(self)

    def recv(self):
        "Read with lock."
        with self.rlock:
            return Tap.recv(self)

    def close(self):
        self.rlock.close()
        self.wlock.close()

# shared locks that will auto-close
# need to be created by caller before fork
class Lock(lockf.Lock):
    "Exclusive bus access."

    __slots__ = ["sock"]

    def __init__(self):
        "Low-level lock from temp fds."
        self.sock = socket.socket(socket.AF_UNIX)
        lockf.Lock.__init__(self, self.sock.fileno())

    def close(self):
        self.sock.close()


def tiptop(size=16384):
    "Create bus ends."
    size = size or 16384
    bus = pair()
    return Tip(bus[1], size, [Lock(), Lock()]), Top(bus[0], size)
