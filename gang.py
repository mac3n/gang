"""
    Context managers for process gang.

    Gang is a process configuration.
    When used as a context it spins up subprocesses
    and creates a message bus.
    Gang send() and recv() operations pass messages between tip and top.
    A Gang has an optional codec to pass python values across the bus.
    Typically, this eould be `pickle`.
"""
import fork
import bus


# tracing
TRACE = None


class GangError(Exception):
    "Protocol Error."
    pass


class Gang(object):
    "A re-useable configuration."

    __slots__ = ["forks", "size", "codec", "pids", "sub", "tap"]

    def __init__(self, forks, codec=None, size=16384):
        "Configure bus, but don't run yet."
        self.forks, self.codec, self.size = forks, codec, size
        # idle
        self.sub = self.tap = None
        self.pids = []

    def __enter__(self):
        "Start up the subprocess bus."
        # shared bus
        (tip, top) = bus.tiptop(size=self.size)
        # fork
        self.pids = []
        self.sub = fork.fork(self.pids, self.forks)
        if not self.sub:
            # bus top
            tip.close()
            self.tap = top
        else:
            # bus tip
            top.close()
            self.tap = tip
            self.tap.tip = self.sub
        return self

    def __exit__(self, etype, evalue, etrace):
        self.tap.close()
        if self.sub is None:
            # top cleanup
            fails = fork.join(self.pids)
            self.pids =[]
            if fails:
                raise GangError("tip failure", fails)
        else:
            if evalue is None:
                # tip cleanup
                fork.join()
        return None

    def send(self, msg):
        if self.codec:
            msg = self.codec.dumps(msg)
        self.tap.send(msg)

    def recv(self):
        msg = self.tap.recv()
        if msg and self.codec:
            msg = self.codec.loads(msg)
        return msg

class GenRed(object):
    """
        Generate/reduce service around map operations.
        This is synchronous: one gen, one red
        Essentially a load balancer.
    """

    __slots__ = ["gang", "gen", "red", "gens", "reds"]

    def __init__(self, gang, gen, red):
        self.gang = gang
        self.gen, self.red = gen, red
        self.gens = self.reds = 0

    def sync(self):
        "Check gen/red synchronization."
        g = self.gang
        if TRACE:
            TRACE("sync %r %d:%d", g.tap, self.gens, self.reds)
        if g.sub and self.gens != self.reds:
            raise GangError("desync %d:%d" % (self.gens, self.reds), g.tap)

    def gen1(self):
        "Tip generates from recv."
        g = self.gang
        while True:
            self.sync()
            item = g.recv()
            if g.tap.fins:
                return
            self.gens += 1
            yield item

    def red1(self, item):
        "Tip red uses send."
        g = self.gang
        self.reds += 1
        self.sync()
        g.send(item)

    def send0(self, item):
        "Top send."
        g = self.gang
        g.send(item)
        self.gens += 1

    def recv0(self):
        "Top recv/red."
        g = self.gang
        item = g.recv()
        if g.tap.fins:
            # early exit
            if TRACE:
                TRACE("break")
                raise GangError("early exit")
        self.red(item)
        self.reds += 1

    def cast0(self):
        "Top generate and reduce indirectly from bus."
        for item in self.gen:
            self.sync()
            if self.gens < self.reds + self.gang.forks:
                # fill tips
                self.send0(item)
            else:
                # keep full
                self.recv0()
                self.send0(item)
        while self.reds < self.gens:
            # drain
            self.sync()
            self.recv0()
        else:
            self.sync()
        if TRACE:
            TRACE("gen done")

    def __enter__(self):
        "start up gang and proxy gen and rad."
        g = self.gang
        if g is None:
            return (self.gen, self.red)
        g.__enter__()
        if TRACE:
            TRACE("enter %r", g.tap)
        if g.sub:
            # sub goes through bus
            return (self.gen1(), self.red1)
        else:
            # top forwards through bus
            return (iter([]), None)

    def __exit__(self, etype, evalue, etrace):
        "Now run the cast loop at Top."
        g = self.gang
        if g is None:
            return None
        if TRACE:
            TRACE("exit %r", g.tap)
        if g.sub:
            if evalue is not None:
                # FIN to pass error to Top
                g.tap.fin()
        else:
            self.cast0()
            g.tap.fin(g.pids)
            while g.tap.fins < g.forks:
                g.recv()
        self.gang.__exit__(etype, evalue, etrace)
        return None
