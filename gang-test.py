from __future__ import print_function

import pickle
import time

import gang

def echo(g, sleep=0):
    msg = g.recv()
    if msg is not None:
        if sleep:
            time.sleep(sleep)
        g.send(msg)
    return msg


def test_gang_io(forks=1,n=1):
    "Bus operation."
    print("serial", "forks:", forks, "items", n)
    data = b"0123456789" * 100
    t = time.time()
    with gang.Gang(forks, size=512) as g:
        if g.sub:
            while echo(g) is not None:
                pass
        else:
            for i in range(n):
                g.send(data)
                m = g.recv()
                assert m == data
            g.tap.fin(g.pids)
    t = time.time() - t
    print("done:", "%.3fs" % t)


def test_gang_tips(forks=1,n=1, sleep=0):
    "Parallel operation."
    print("parallel", "forks:", forks, "items", n, "sleep:", sleep)
    data = b"0123456789" * 100
    t = time.time()
    with gang.Gang(forks, size=512) as g:
        if g.sub:
            while echo(g) is not None:
                pass
        else:
            i= 0
            busy = 0
            # fill up tips
            while busy < forks and i < n:
                g.send(data)
                busy += 1
                i += 1
            # keep full
            while i < n:
                m = g.recv()
                assert m == data
                g.send(data)
                i += 1
            # drain tips
            while 0 < busy:
                m = g.recv()
                assert m == data
                busy -= 1
            g.tap.fin(g.pids)
    t = time.time() - t
    print("done:", "%.3fs" % t)


def test_codec(forks=1, n=10):
    "Test data codec."
    with gang.Gang(forks, codec=pickle, size=512) as g:
        if g.sub:
            while echo(g) is not None:
                pass
        else:
            for i in range(n):
                data = dict((str(v),  v) for v in range(i))
                g.send(data)
                m = g.recv()
                print(m)
                assert m == data
            g.tap.fin(g.pids)


if __name__ == "__main__":
    import bus
    import fork
    import trace

    gang.TRACE = bus.TRACE = fork.TRACE = trace.trace

    test_gang_io()
    test_gang_io(forks=2, n=2)
    test_gang_io(forks=10, n=10)
    test_gang_tips(forks=25, n=50, sleep=1)
    test_codec()
