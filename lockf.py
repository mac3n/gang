"""
    wrap fcntl.lockf for python2
    using fcntl, but opcodes from flock(?!)
    it's defined as os.lockf in python3 and uses lockf codes

    we use lockf instead of, e.g., flock, because we don't want to share locks
    on inherited file descriptors
"""

import fcntl


# tracing
TRACE = None


def lockf(fd, test=None):
    "Lock fd exclusive."
    op = fcntl.LOCK_EX | fcntl.LOCK_NB if test else fcntl.LOCK_EX
    fcntl.lockf(fd, op)
    if TRACE:
        TRACE("lockf(%d)", fd)

def ulockf(fd):
    "Release fd lock."
    if TRACE:
        TRACE("ulockf(%d)", fd)
    fcntl.lockf(fd, fcntl.LOCK_UN)

# make a context so we can 'with Lock'

class Lock(object):
    "Exclusive lock context manager."
    __slots__ = ["fd"]
    def __init__(self, fd):
        self.fd = fd
        if TRACE:
            TRACE("Lock(%d)", fd)

    def __enter__(self):
        lockf(self.fd)

    def __exit__(self, *args):
        ulockf(self.fd)
        return None

