"""
    Test lock
    we do this across forks, so there's scaffolding to coordinate
    lock operations across processes
"""

import socket

import fork
import lockf


# tracing
TRACE = None


# don't use nosetests here, as we fork processes

def lockftest():
    "Verify lockf behavior."
    # our remote lock protocol
    ULOCK, LOCK, TEST, DONE, FAIL = b'U', b'L', b'T', b'Y', b'N'
    # make some channels
    (sup1, sub1) = socket.socketpair(socket.AF_UNIX, socket.SOCK_SEQPACKET)
    (sup2, sub2) = socket.socketpair(socket.AF_UNIX, socket.SOCK_SEQPACKET)
    # and just for the shared fd
    lk = sup1.fileno()
    # fork two subs and have each use a channel
    pids = []
    child = fork.fork(pids, 2)
    sub = sub1 if child == 1 else sub2 if child == 2 else None
    if sub:
        # child applies L/T/U lock op to shared fd & reports result
        while True:
            op = sub.recv(1024)
            try:
                if op == ULOCK:
                    lockf.ulockf(lk)
                elif op == LOCK:
                    lockf.lockf(lk)
                elif op == TEST:
                    lockf.lockf(lk, test=True)
                else:
                    break
            except (IOError, OSError) as e:
                if TRACE:
                    TRACE("fail: %s", e)
                sub.send(FAIL)
            else:
                if TRACE:
                    TRACE("done")
                sub.send(DONE)
        fork.join()
    else:
        # parent process
        # sub1 locks
        if TRACE:
            TRACE("p1: lock")
        sup1.send(LOCK)
        r1 = sup1.recv(1024)
        # sub2 tests
        if TRACE:
            TRACE("p2: test")
        sup2.send(TEST)
        r2 = sup2.recv(1024)
        assert r1 == DONE
        assert r2 == FAIL
        # p1 release
        if TRACE:
            TRACE("p1: ulock")
        sup1.send(ULOCK)
        r1 = sup1.recv(1024)
        # p2 tests
        if TRACE:
            TRACE("p2: test")
        sup2.send(TEST)
        r2 = sup2.recv(1024)
        assert r1 == DONE
        assert r2 == DONE
        # clean up
        sup1.send(DONE)
        sup2.send(DONE)
        fork.join(pids)


if __name__ == "__main__":
    import trace

    TRACE = lockf.TRACE = trace.trace
    lockftest()
