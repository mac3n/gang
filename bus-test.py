"""
    Bus tap tests
"""
import time

import bus
import fork

def test_tap0():
    "Test tap pair."
    p = bus.pair()
    (r, w) = bus.Tap(p[0], 128), bus.Tap(p[1], 128)
    msg = b"hello, world."
    # message
    w.send(msg)
    got = r.recv()
    assert got == msg
    msg += b"(2)"
    w.send(msg)
    got = r.recv()
    assert got == msg
    assert not r.fins
    # FIN
    w.fin()
    got = r.recv()
    assert got is None
    assert r.fins
    r.close()
    w.close()

def test_tap1():
    "Test packetizing."
    patt = b"0123456789"
    p = bus.pair()
    (r, w) = bus.Tap(p[0], 4), bus.Tap(p[1], 4)
    # packetized messages
    for n in range(len(patt)):
        w.send(patt[:n + 1])
        got = r.recv()
        assert got == patt[:n + 1]
        assert not r.fins
    # FIN
    w.fin()
    got = r.recv()
    assert got is None
    assert r.fins
    r.close()
    w.close()


def test_tip0(n=2):
    "Test tip locking."
    (tip, top) = bus.tiptop(size=4)
    # fork tips
    pids = []
    sub = fork.fork(pids, n)
    if sub:
        # tip echoes multipart message
        top.close()
        tip.tip = sub
        while not tip.fins:
            time.sleep(1)
            r = tip.recv()
            if r:
                tip.send(r)
        tip.fin()
        fork.join()
    else:
        # top sends multipart message
        m = b'0123456789' * 5
        tip.close()
        top.send(m)
        top.fin(pids)
        while top.fins < n:
            r = top.recv()
            # message must echo intact
            if r:
                assert r == m
        fork.join(pids)

def test_tip1(n=10):
    "Test many tips."
    (tip, top) = bus.tiptop()
    # fork tips
    pids = []
    sub = fork.fork(pids, n)
    if sub:
        # tip echoes 1 message
        top.close()
        tip.tip = sub
        r = tip.recv()
        if r:
            tip.send(r)
        tip.fin()
        fork.join()
    else:
        # top sends tips message
        m = b'0123456789' * 5
        tip.close()
        for _ in pids:
            top.send(m)
        while top.fins < n:
            r = top.recv()
            # message must echo intact
            if r:
                assert r == m
        fork.join(pids)


if __name__ == "__main__":
    import trace
    import lockf

    bus.TRACE = fork.TRACE = lockf.TRACE = trace.trace
    test_tap0()
    test_tap1()
    test_tip0()
    test_tip1(100)
