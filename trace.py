"""
    Utility trace function
"""
from __future__ import print_function

import os
import sys
import time

import fork

# start time
t0 = None


def trace(msg, *args):
    global t0
    if t0 is None:
        t0 = time.time()
    print("%.6f" % (time.time() - t0), os.getpid(), msg % args, sep="\t", file=sys.stderr)
